package br.com.bd.projeto3;

import java.awt.EventQueue;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/* Para gerar pdf */
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

public class Projeto3 {

	/**
	 * Launch the application.
	 */

	 public static void gerarRelatorioPdf(ResultSet set, String reportName) throws SQLException, FileNotFoundException, DocumentException
	 {
		ResultSetMetaData setMetaData = set.getMetaData();
		Document relatorio = new Document();
		PdfWriter.getInstance(relatorio, new FileOutputStream(reportName));
		relatorio.open();
		int nCol = setMetaData.getColumnCount();
		PdfPTable tabelaRelatorio = new PdfPTable(setMetaData.getColumnCount());

		String [] labels = new String[nCol];
		for (int i = 1; i <= nCol; ++i ) {
			labels[i-1] = setMetaData.getColumnName(i);
			tabelaRelatorio.addCell(new PdfPCell(new Phrase(setMetaData.getColumnLabel(i))));
		}

		while (set.next()) {
			for(int i = 1; i <= nCol; ++i)
				tabelaRelatorio.addCell(new PdfPCell(new Phrase(set.getString(labels[i-1]))));
		}

		/* Inserir tabela de relatorio no PDF */
		relatorio.add(tabelaRelatorio);
		relatorio.close();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StateMachine.getInstance().makeTransistion(State.MAIN);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
