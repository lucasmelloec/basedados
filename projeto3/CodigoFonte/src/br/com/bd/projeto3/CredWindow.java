package br.com.bd.projeto3;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class CredWindow implements Window{
	private JFrame frmProjeto;
	
	
	/**
	 * Create the application.
	 */
	public CredWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmProjeto = new JFrame();
		frmProjeto.setResizable(false);
		frmProjeto.setTitle("Projeto 3 - Base de Dados");
		
		frmProjeto.setBounds(100, 50, 800, 600);
		frmProjeto.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 794, 571);
		panel.setLayout(null);
		frmProjeto.getContentPane().add(panel);
		
		JTextPane txtCredits = new JTextPane();
		txtCredits.setFont(new Font("Arial", Font.BOLD, 11));
		txtCredits.setEditable(false);
		txtCredits.setText("\r\n\r\n\r\n\r\n\r\n                                Universidade de S\u00E3o Paulo - Campus S\u00E3o Carlos\r\n                                                 Engenharia de Computa\u00E7\u00E3o\r\n\r\n\r\n\r\n\r\n\r\n                   Caio C\u00E9sar de Almeida Guimar\u00E3es                              8551497\r\n\r\n                   Henrique Cintra Miranda de Souza Aranha                  8551434\r\n\r\n                   Lucas Eduardo Carreiro de Mello                                  8504218\r\n\r\n\r\n\r\n\r\n\r\n                                                     Novembro de 2015");
		txtCredits.setBounds(10, 11, 642, 461);
		panel.add(txtCredits);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setFont(new Font("Arial", Font.BOLD, 11));
		btnVoltar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				StateMachine.getInstance().makeTransistion(State.MAIN);
			}
		});
		btnVoltar.setBounds(656, 449, 128, 23);
		panel.add(btnVoltar);
	}

	public JFrame getFrame() {
		return frmProjeto;
	}

	public void setFrame(JFrame frame) {
		this.frmProjeto = frame;
	}
}
