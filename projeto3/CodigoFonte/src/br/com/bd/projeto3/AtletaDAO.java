package br.com.bd.projeto3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/* 
 * Esta classe utiliza o padrao de projeto Data Acess Object
 * Sera o intermediario responsavel por acessar os dados referentes a classe Local no banco de dados
 */
public class AtletaDAO {
	private Connection connection;
	PreparedStatement stmt;
	
	/*Construtor*/
	public AtletaDAO(){
		ConnectionFactory connectionfactory = new ConnectionFactory();
		this.connection = connectionfactory.getConnection();
	}

	/*Listagem*/
	public List<Atleta> listar(){
		try {
			String sql = "SELECT RG, Nome, CPF, Sexo, DataDeNascimento,"
					+ "	Altura, Peso, Atletica, COUNT(DISTINCT Evento) AS \"Eventos Participados\""
					+ "FROM ("
					+ "(SELECT A.Nome, A.RG, A.CPF, A.Sexo, A.DataDeNascimento,"
					+ "	A.Altura, A.Peso, A.Atletica, Atr.Evento"
					+ "	FROM Atracao Atr JOIN Jogo J"
					+ " ON Atr.Evento = J.Evento"
					+ " JOIN EquipeAtleta EA"
					+ " ON J.Equipe1 = EA.Equipe OR J.Equipe2 = EA.Equipe"
					+ " JOIN Atleta A ON A.RG = EA.Atleta)"
					+ " UNION"
					+ " (SELECT A.Nome, A.RG, A.CPF, A.Sexo, A.DataDeNascimento,"
					+ "	A.Altura, A.Peso, A.Atletica, Atr.Evento"
					+ "	FROM Atracao Atr JOIN Etapa E"
					+ " ON Atr.Evento = E.Evento"
					+ " JOIN Atleta A ON E.Atleta = A.RG)) Atleta"
					+ " GROUP BY RG, Nome, CPF, Sexo, DataDeNascimento,"
					+ "	Altura, Peso, Atletica"
					+ " HAVING COUNT(DISTINCT Evento) >= 3";

			List<Atleta> atletaList = new ArrayList<Atleta>();
			
			stmt = this.connection.prepareStatement(sql);
			ResultSet set = stmt.executeQuery();
			
			
			while(set.next()){
				Atleta atleta = new Atleta();
				atleta.setRg(set.getString("RG"));
				atleta.setNome(set.getString("Nome"));
				atleta.setCpf(set.getString("CPF"));
				atleta.setSexo(set.getString("Sexo"));
				atleta.setData(set.getString("DataDeNascimento"));
				atleta.setAltura(set.getInt("Altura"));
				atleta.setPeso(set.getInt("Peso"));
				atleta.setAtletica(set.getInt("Atletica"));
				atleta.setEventos(set.getInt("Eventos Participados"));
				
				atletaList.add(atleta);
			}
			set.close();
			stmt.close();
			return atletaList;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void gerarRelatorioPdf() {
		try {
			String sql = "SELECT RG, Nome, CPF, Sexo, DataDeNascimento,"
					+ "	Altura, Peso, Atletica, COUNT(DISTINCT Evento) AS \"Eventos Participados\""
					+ "FROM ("
					+ "(SELECT A.Nome, A.RG, A.CPF, A.Sexo, A.DataDeNascimento,"
					+ "	A.Altura, A.Peso, A.Atletica, Atr.Evento"
					+ "	FROM Atracao Atr JOIN Jogo J"
					+ " ON Atr.Evento = J.Evento"
					+ " JOIN EquipeAtleta EA"
					+ " ON J.Equipe1 = EA.Equipe OR J.Equipe2 = EA.Equipe"
					+ " JOIN Atleta A ON A.RG = EA.Atleta)"
					+ " UNION"
					+ " (SELECT A.Nome, A.RG, A.CPF, A.Sexo, A.DataDeNascimento,"
					+ "	A.Altura, A.Peso, A.Atletica, Atr.Evento"
					+ "	FROM Atracao Atr JOIN Etapa E"
					+ " ON Atr.Evento = E.Evento"
					+ " JOIN Atleta A ON E.Atleta = A.RG)) Atleta"
					+ " GROUP BY RG, Nome, CPF, Sexo, DataDeNascimento,"
					+ "	Altura, Peso, Atletica"
					+ " HAVING COUNT(DISTINCT Evento) >= 3";
			
			stmt = this.connection.prepareStatement(sql);
			ResultSet set = stmt.executeQuery();
			
			Projeto3.gerarRelatorioPdf(set, "RelatorioAtletas.pdf");
			
			set.close();
			stmt.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		return connection;
	}
}
