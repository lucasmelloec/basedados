package br.com.bd.projeto3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/* 
 * Esta classe utiliza o padrão de projeto Data Acess Object
 * Sera o intermediario responsavel por acessar os dados referentes a classe Local no banco de dados
 */
public class LocalDAO {
	private Connection connection;
	PreparedStatement stmt;
	
	/*Construtor*/
	public LocalDAO(){
		ConnectionFactory connectionfactory = new ConnectionFactory();
		this.connection = connectionfactory.getConnection();
	}

	/*Insercao*/
	public void inserir(Local local){
		try {
			String sql = "INSERT INTO Local " + "(ID,Denominacao,Endereco)" + "VALUES (?,?,?)";
			stmt = this.connection.prepareStatement(sql);
			
			stmt.setInt(1,local.getId());
			stmt.setString(2,local.getDenominacao());
			stmt.setInt(3,local.getEndereco());
			
			stmt.execute();
			stmt.close();
			
		} catch(Exception e){
			e.printStackTrace();
		}		
	}

	/*Listagem*/
	public List<Local> listar(){
		try {
			String sql = "SELECT * FROM Local";
			List<Local> localList = new ArrayList<Local>();
			
			stmt = this.connection.prepareStatement(sql);
			ResultSet set = stmt.executeQuery();
			
			
			while(set.next()){
				Local local = new Local();
				local.setId(set.getInt("ID"));
				local.setEndereco(set.getInt("Endereco"));
				local.setDenominacao(set.getString("Denominacao"));
				
				localList.add(local);
			}
			set.close();
			stmt.close();
			return localList;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/*Atualizacao*/
	public void atualizar(Local local){
		try{
			String sql = "UPDATE Local SET denominacao = ?, endereco = ? WHERE id = ?";
			stmt = this.connection.prepareStatement(sql);
			
			stmt.setString(1, local.getDenominacao());
			stmt.setInt(2, local.getEndereco());
			stmt.setInt(3, local.getId());
			
			stmt.execute();
			stmt.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/*Remocao*/
	public void remover(Local local){
		try{
			String sql = "DELETE FROM Local WHERE id=?";
			stmt = this.connection.prepareStatement(sql);
			
			stmt.setInt(1, local.getId());
			
			stmt.execute();
			stmt.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public List<Integer> getEnderecosValidos() {
		try {
			String sql = "SELECT ID FROM Endereco";
			List<Integer> enderecoList = new ArrayList<Integer>();
			
			stmt = this.connection.prepareStatement(sql);
			ResultSet set = stmt.executeQuery();
			
			while(set.next()){
				enderecoList.add(set.getInt("ID"));
			}
			
			set.close();
			stmt.close();
			return enderecoList;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public Connection getConnection() {
		return connection;
	}
}
