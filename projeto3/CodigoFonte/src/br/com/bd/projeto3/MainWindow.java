package br.com.bd.projeto3;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;

public class MainWindow implements Window{
	private JFrame frmProjeto;

	private LocalDAO localDAO;
	private List<Local> locais;
	private List<Integer> enderecos;

	/**
	 * Create the application.
	 */
	public MainWindow() {
		localDAO = new LocalDAO();

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmProjeto = new JFrame();
		frmProjeto.setResizable(false);
		frmProjeto.setTitle("Projeto 3 - Base de Dados");

		frmProjeto.setBounds(100, 50, 800, 600);
		frmProjeto.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 794, 571);

		JList<String> list = new JList<String>();
		list.setFont(new Font("Arial", Font.BOLD, 11));
		list.setBounds(10, 55, 635, 446);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JTextField textDenominacao = new JTextField();
		textDenominacao.setFont(new Font("Arial", Font.BOLD, 11));
		textDenominacao.setToolTipText("Denomina\u00E7\u00E3o");
		textDenominacao.setBounds(148, 537, 163, 22);
		textDenominacao.setColumns(10);

		JComboBox<Integer> comboEndereco = new JComboBox<Integer>();
		comboEndereco.setFont(new Font("Arial", Font.BOLD, 11));
		comboEndereco.setToolTipText("Endere\u00E7o");
		comboEndereco.setBounds(321, 537, 131, 22);

		JButton btnInserir = new JButton("Inserir");
		btnInserir.setFont(new Font("Arial", Font.BOLD, 11));
		btnInserir.setBounds(462, 537, 131, 23);

		JButton btnModificar = new JButton("Modificar");
		btnModificar.setFont(new Font("Arial", Font.BOLD, 11));
		btnModificar.setBounds(655, 52, 128, 23);
		panel.setLayout(null);
		panel.add(textDenominacao);
		panel.add(comboEndereco);
		panel.add(btnInserir);
		panel.add(list);
		panel.add(btnModificar);
		frmProjeto.getContentPane().add(panel);

		JButton btnRemover = new JButton("Remover");
		btnRemover.setFont(new Font("Arial", Font.BOLD, 11));
		btnRemover.setBounds(655, 80, 128, 23);
		panel.add(btnRemover);

		JLabel lblId = new JLabel("       ID");
		lblId.setFont(new Font("Arial", Font.BOLD, 11));
		lblId.setBounds(42, 512, 46, 14);
		panel.add(lblId);

		JLabel lblDenominao = new JLabel("Denomina\u00E7\u00E3o");
		lblDenominao.setFont(new Font("Arial", Font.BOLD, 11));
		lblDenominao.setBounds(188, 512, 86, 14);
		panel.add(lblDenominao);

		JLabel lblEndereo = new JLabel("Endere\u00E7o(ID)");
		lblEndereo.setFont(new Font("Arial", Font.BOLD, 11));
		lblEndereo.setBounds(360, 512, 92, 14);
		panel.add(lblEndereo);

		JButton btnCreditos = new JButton("Cr\u00E9ditos");
		btnCreditos.setFont(new Font("Arial", Font.BOLD, 11));
		btnCreditos.setBounds(655, 478, 128, 23);
		panel.add(btnCreditos);

		JButton btnListarAtletas = new JButton("Listar Atletas");
		btnListarAtletas.setFont(new Font("Arial", Font.BOLD, 11));
		btnListarAtletas.setBounds(656, 450, 127, 23);
		panel.add(btnListarAtletas);

		JLabel labelStatus = new JLabel("");
		labelStatus.setFont(new Font("Arial", Font.BOLD, 11));
		labelStatus.setToolTipText("Status");
		labelStatus.setBounds(599, 537, 184, 22);
		panel.add(labelStatus);

		JSpinner spinnerID = new JSpinner();
		spinnerID.setFont(new Font("Arial", Font.BOLD, 11));
		spinnerID.setModel(new SpinnerNumberModel(0, 0, 2147483647, 1));
		spinnerID.setBounds(10, 538, 128, 20);
		panel.add(spinnerID);

		JLabel lblListColumns = new JLabel("                     ID                     |                     Denomina\u00E7\u00E3o                     |                     Endere\u00E7o");
		lblListColumns.setFont(new Font("Arial", Font.BOLD, 11));
		lblListColumns.setBounds(10, 30, 635, 14);
		panel.add(lblListColumns);

		btnInserir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if(btnInserir.getText().equals("Inserir")) {
					boolean error = false;

					for (Local it : locais) {
						if(it.getId() == spinnerID.getValue()) {
							error = true;
							labelStatus.setText("ID ja existe!");
						}
					}
					
					if(textDenominacao.getText() == null || textDenominacao.getText().equals("")) {
						error = true;
						labelStatus.setText("Denominacao vazia!");
					}

					if (error == true) {
						labelStatus.setForeground(Color.RED);
					}
					else {
						Local local = new Local();
						local.setId((Integer) spinnerID.getValue());
						local.setDenominacao(textDenominacao.getText());
						local.setEndereco((Integer) comboEndereco.getSelectedItem());

						localDAO.inserir(local);

						labelStatus.setText("Sucesso!");
						labelStatus.setForeground(Color.GREEN);
					}
				}
				else {
					if(textDenominacao.getText() == null || textDenominacao.getText().equals("")) {
						labelStatus.setText("Denominacao vazia!");
						labelStatus.setForeground(Color.RED);
					}
					else {
						Local local = new Local();
						local.setId((Integer) spinnerID.getValue());
						local.setDenominacao(textDenominacao.getText());
						local.setEndereco((Integer) comboEndereco.getSelectedItem());
	
						localDAO.atualizar(local);
	
						btnInserir.setText("Inserir");
						spinnerID.setEnabled(true);
						
						labelStatus.setText("Sucesso!");
						labelStatus.setForeground(Color.GREEN);
					}
				}

				updateList(list);
				updateComboBox(comboEndereco);
			}
		});

		btnModificar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				labelStatus.setText("");
				
				Local local;

				if(list.getSelectedIndex() == -1)
					local = null;
				else
					local = locais.get(list.getSelectedIndex());
				
				if(local != null) {
					spinnerID.setValue(local.getId());
					spinnerID.setEnabled(false);
	
					textDenominacao.setText(local.getDenominacao());
	
					comboEndereco.setSelectedItem(local.getEndereco());
	
					btnInserir.setText("Atualizar");
				}
				else {
					labelStatus.setText("Selecione uma tupla acima");
					labelStatus.setForeground(Color.RED);
				}
			}
		});

		btnRemover.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				labelStatus.setText("");

				Local local;

				if(list.getSelectedIndex() == -1)
					local = null;
				else
					local = locais.get(list.getSelectedIndex());
				
				if(local != null) {
					localDAO.remover(local);
	
					updateList(list);
					updateComboBox(comboEndereco);
				}
				else {
					labelStatus.setText("Selecione uma tupla acima");
					labelStatus.setForeground(Color.RED);
				}
			}
		});

		btnCreditos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				StateMachine.getInstance().makeTransistion(State.CREDITS);
			}
		});

		btnListarAtletas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				StateMachine.getInstance().makeTransistion(State.LIST);
			}
		});

		frmProjeto.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	try {
		    		localDAO.getConnection().close();
		    	} catch(Exception e) {
		    		e.printStackTrace();
		    	}
		    }
		});

		updateList(list);
		updateComboBox(comboEndereco);
		
		JLabel lblLocal = new JLabel("                                                                                           Tabela de locais");
		lblLocal.setFont(new Font("Arial", Font.BOLD, 11));
		lblLocal.setBounds(10, 5, 635, 14);
		panel.add(lblLocal);
	}

	private void updateComboBox(JComboBox<Integer> comboEndereco) {
		enderecos = localDAO.getEnderecosValidos();

		Integer [] aux = new Integer[enderecos.size()];
		enderecos.toArray(aux);

		comboEndereco.setModel(new DefaultComboBoxModel<Integer>(aux));
	}

	private void updateList(JList<String> list) {
		locais = localDAO.listar();

		List<String> auxList = new ArrayList<String>();

		for(Local it : locais) {
			auxList.add("                     " + it.getId().toString() + "                     |                 " + it.getDenominacao() + "                 |                 " +  it.getEndereco().toString());
		}

		list.setModel(new AbstractListModel<String>() {
			private static final long serialVersionUID = 1L;

			public List<String> values = auxList;

			public int getSize() {
				return values.size();
			}
			public String getElementAt(int index) {
				return values.get(index);
			}
		});
	}

	public JFrame getFrame() {
		return frmProjeto;
	}

	public void setFrame(JFrame frame) {
		this.frmProjeto = frame;
	}
}
