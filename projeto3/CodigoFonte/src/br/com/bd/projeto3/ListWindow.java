package br.com.bd.projeto3;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.Font;

public class ListWindow implements Window{
	private JFrame frmProjeto;
	
	private AtletaDAO atletaDAO;
	private List<Atleta> atletas;
	
	
	/**
	 * Create the application.
	 */
	public ListWindow() {
		atletaDAO = new AtletaDAO();
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmProjeto = new JFrame();
		frmProjeto.setResizable(false);
		frmProjeto.setTitle("Projeto 3 - Base de Dados");
		
		frmProjeto.setBounds(100, 50, 800, 600);
		frmProjeto.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 794, 571);
		panel.setLayout(null);
		frmProjeto.getContentPane().add(panel);
		
		JButton btnVoltar2 = new JButton("Voltar");
		btnVoltar2.setFont(new Font("Arial", Font.BOLD, 11));
		btnVoltar2.setBounds(655, 477, 128, 23);
		panel.add(btnVoltar2);
		
		JLabel labelStatus = new JLabel("");
		labelStatus.setToolTipText("Status");
		labelStatus.setBounds(406, 541, 46, 14);
		panel.add(labelStatus);
		
		JButton btnRelatorio = new JButton("Relatorio");
		btnRelatorio.setFont(new Font("Arial", Font.BOLD, 11));
		btnRelatorio.setBounds(655, 52, 128, 23);
		panel.add(btnRelatorio);
		
		JLabel lblListar = new JLabel("                                         Lista de atletas que participaram de 3 ou mais eventos");
		lblListar.setFont(new Font("Arial", Font.BOLD, 11));
		lblListar.setBounds(10, 11, 773, 14);
		panel.add(lblListar);
		
		JLabel lblRgNome = new JLabel("    RG    |    Nome    |    CPF    |    Sexo    |    Data de Nascimento    |    Altura    |    Peso    |    Atletica(ID)    |    Eventos Participados");
		lblRgNome.setFont(new Font("Arial", Font.BOLD, 11));
		lblRgNome.setBounds(10, 30, 773, 14);
		panel.add(lblRgNome);
		
		JList<String> list = new JList<String>();
		list.setEnabled(false);
		list.setFont(new Font("Arial", Font.BOLD, 11));
		list.setBounds(10, 55, 635, 445);
		panel.add(list);
		
		btnVoltar2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				StateMachine.getInstance().makeTransistion(State.MAIN);
			}
		});
		
		btnRelatorio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				atletaDAO.gerarRelatorioPdf();
				
				JOptionPane.showMessageDialog(null, "Relatorio salvo como RelatorioAtletas.pdf", "Sucesso",
                        JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		frmProjeto.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	try {
		    		atletaDAO.getConnection().close();
		    	} catch(Exception e) {
		    		e.printStackTrace();
		    	}
		    }
		});
		
		updateText(list);
	}
	
	private void updateText(JList<String> list) {
		atletas = atletaDAO.listar();
		if(atletas == null)
			return;

		List<String> auxList = new ArrayList<String>();

		for(Atleta it : atletas) {
			auxList.add("  " + it.getRg() + "  |  " + it.getNome() + "  |  " +  it.getCpf() + "  |  " +  it.getSexo() + "  |  " +  it.getData() + "  |  " +  it.getAltura().toString() + "  |  " +  it.getPeso().toString() + "  |  " +  it.getAtletica().toString() + "  |  " +  it.getEventos().toString());
		}

		list.setModel(new AbstractListModel<String>() {
			private static final long serialVersionUID = 1L;

			public List<String> values = auxList;

			public int getSize() {
				return values.size();
			}
			public String getElementAt(int index) {
				return values.get(index);
			}
		});
	}

	public JFrame getFrame() {
		return frmProjeto;
	}

	public void setFrame(JFrame frame) {
		this.frmProjeto = frame;
	}
}
