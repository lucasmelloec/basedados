package br.com.bd.projeto3;

public class StateMachine {
	private static StateMachine INSTANCE;
	
	private State state;
	Window window;
	
	private StateMachine() {
		state = null;
		window = null;
	}
	
	public static synchronized StateMachine getInstance() {
		if(INSTANCE == null)
			INSTANCE = new StateMachine();
		
		return INSTANCE;
	}
	
	public void makeTransistion(State newState) {
		state = newState;
		
		if(window != null)
			window.getFrame().setVisible(false);
		
		switch(state) {
			case MAIN:
				window = new MainWindow();
				break;
			case CREDITS:
				window = new CredWindow();
				break;
			case LIST:
				window = new ListWindow();
				break;
			default:
				break;
		}
		window.getFrame().setVisible(true);
	}
}
