package br.com.bd.projeto3;

public class Atleta {
	private String rg;
	private String nome;
	private String cpf;
	private String sexo;
	private String data;
	private Integer altura;
	private Integer peso;
	private Integer atletica;
	private Integer eventos;
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Integer getAltura() {
		return altura;
	}
	public void setAltura(Integer altura) {
		this.altura = altura;
	}
	public Integer getPeso() {
		return peso;
	}
	public void setPeso(Integer peso) {
		this.peso = peso;
	}
	public Integer getAtletica() {
		return atletica;
	}
	public void setAtletica(Integer atletica) {
		this.atletica = atletica;
	}
	public Integer getEventos() {
		return eventos;
	}
	public void setEventos(Integer eventos) {
		this.eventos = eventos;
	}
	
}
