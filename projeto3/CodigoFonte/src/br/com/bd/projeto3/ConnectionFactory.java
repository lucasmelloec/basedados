package br.com.bd.projeto3;

import java.sql.Connection;
import java.sql.DriverManager;

/* 
 * Esta classe utiliza o padrão de projeto Factory Method
 * Ela será a responsável por fabricar os objetos do tipo Connection para se fazer conexão com o banco de dados
 */

public class ConnectionFactory{
    public Connection getConnection(){
        try {
        	Class.forName("oracle.jdbc.driver.OracleDriver");
            return DriverManager.getConnection(
                    "jdbc:oracle:thin:@grad.icmc.usp.br:15215:orcl",
                    "8504218",
                    "batata");
        } catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
}