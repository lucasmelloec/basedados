package br.com.bd.projeto3;

import javax.swing.JFrame;

public interface Window {
	public JFrame getFrame();

	public void setFrame(JFrame frame);
}
