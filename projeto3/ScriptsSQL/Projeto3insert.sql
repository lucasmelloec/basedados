INSERT INTO Evento
VALUES (0, 'TUSCA', 'Torneio universit�rio de S�o Carlos', NULL);
INSERT INTO Evento
VALUES (1, 'InterUNESP', 'Torneio universit�rio entre os campi UNESP', NULL);
INSERT INTO Evento
VALUES (2, 'Tusquinha', NULL, NULL);

INSERT INTO TipoIngresso
VALUES ('masculino');
INSERT INTO TipoIngresso
VALUES ('feminino');
INSERT INTO TipoIngresso
VALUES ('meia');
INSERT INTO TipoIngresso
VALUES ('promocional');

INSERT INTO Universidade
VALUES (0, 'USP', 'S�o Paulo', 'S�o Paulo');
INSERT INTO Universidade
VALUES (1, 'UNESP', 'Araraquara', 'S�o Paulo');
INSERT INTO Universidade
VALUES (2, 'UNICAMP', 'Campinas', 'S�o Paulo');

INSERT INTO Endereco
VALUES (0, 'Rua das Rosas', 'Jardim', 10);
INSERT INTO Endereco
VALUES (1, 'Rua das Margaridas', 'Jardim', 20);
INSERT INTO Endereco
VALUES (2, 'Rua Fulano', 'Beltrano', 30);

INSERT INTO Atletica
VALUES (0, 'CAASO', NULL, 'Vai CAASO!');
INSERT INTO Atletica
VALUES (1, 'UNESP', NULL, NULL);
INSERT INTO Atletica
VALUES (2, 'UNICAMP', NULL, NULL);

INSERT INTO Atleta
VALUES ('111111111', '111.222.333-44', 'Jo�o', 'masculino', '11/01/1992', 178, 70, 0);
INSERT INTO Atleta
VALUES ('222222222', '555.666.777-88', 'Alice', 'feminino', '20/03/1993', 168, 68, 1);
INSERT INTO Atleta
VALUES ('333333333', '999.888.777-66', 'Roberto', 'masculino', '11/01/1991', 180, 78, 2);

INSERT INTO Cliente
VALUES ('444444444', '123.456.789-00', 'Brenda', 'feminino', '03/04/1994', 'S�o Carlos', 'S�o Paulo', 'brenda@email.com', '123456');
INSERT INTO Cliente
VALUES ('555555555', '122.336.449-21', 'Maria', 'feminino', '05/06/1995', 'Campinas', 'S�o Paulo', 'maria@email.com', '111222');
INSERT INTO Cliente
VALUES ('666666666', '152.577.473-76', 'Rodrigo', 'masculino', '15/09/1993', 'S�o Carlos', 'S�o Paulo', 'rodrigo@email.com', 'senha1');

INSERT INTO Compra
VALUES (0, '444444444', '11/07/2015', 100.00, 100.00, 'Boleto', '20/07/2015', NULL, NULL);
INSERT INTO Compra
VALUES (1, '555555555', '09/09/2015', 60.50, 60.50, 'CartaoDeCredito', '18/05/2019', 9876, 'Visa');
INSERT INTO Compra
VALUES (2, '666666666', '11/07/2015', 100.00, 100.00, 'Boleto', '20/07/2015', NULL, NULL);

INSERT INTO Modalidade
VALUES (0, 'Nata��o', 'Quem consegue nadar 20 metros mais rapidamente', 'B');
INSERT INTO Modalidade
VALUES (1, 'V�lei', 'Campeonato de v�leibol', 'A');
INSERT INTO Modalidade
VALUES (2, '100 Metros', '', 'B');

INSERT INTO Local
VALUES (0, 'Local dos jogos do TUSCA', 0);
INSERT INTO Local
VALUES (1, 'Local dos jogos do tusquinha', 2);
INSERT INTO Local
VALUES (2, 'Local dos jogos do INTERUNESP', 1);

INSERT INTO Ingresso
VALUES (0, 0, 100.00, 1);
INSERT INTO Ingresso
VALUES (1, 1, 60.50, 1);
INSERT INTO Ingresso
VALUES (2, 2, 50.00, 2);

INSERT INTO Atracao
VALUES (0, 0, 0, '18:00', '16Anos', NULL, '20/10/2015');
INSERT INTO Atracao
VALUES (1, 1, 2, '19:00', '18Anos', NULL, '25/10/2015');
INSERT INTO Atracao
VALUES (2, 2, 1, '16:00', 'Livre', NULL, '20/10/2015');

INSERT INTO Equipe
VALUES (0, 0, 1, 2, 0, 0, 'Grupo 1');
INSERT INTO Equipe
VALUES (1, 1, 1, 0, 2, 0, 'Grupo 1');
INSERT INTO Equipe
VALUES (2, 2, 1, 1, 0, 0, 'Grupo 1');

INSERT INTO Jogo
VALUES (0, 0, 1, '20/10/2015', '14:00', '03x00', 'Equipe 0 ganhou', 0, 0);
INSERT INTO Jogo
VALUES (1, 0, 2, '23/10/2015', '16:00', '01x02', 'Equipe 2 ganhou', 0, 0);
INSERT INTO Jogo
VALUES (2, 0, 1, '23/10/2015', '17:00', '03x00', 'Equipe 0 ganhou', 0, 0);
INSERT INTO Jogo
VALUES (3, 0, 1, '20/11/2015', '13:00', '03x00', 'Equipe 0 ganhou', 0, 1);
INSERT INTO Jogo
VALUES (4, 0, 2, '20/12/2015', '16:00', '03x00', 'Equipe 0 ganhou', 0, 2);

INSERT INTO AtletaModalidade
VALUES ('111111111', 0, '01:30:12');
INSERT INTO AtletaModalidade
VALUES ('111111111', 2, '02:30:12');
INSERT INTO AtletaModalidade
VALUES ('333333333', 0, '01:40:17');

INSERT INTO Etapa
VALUES (0, '111111111', 0, '25/10/2015', '09:00', 0, 0);
INSERT INTO Etapa
VALUES (1, '111111111', 2, '26/10/2015', '11:00', 0, 0);
INSERT INTO Etapa
VALUES (2, '333333333', 0, '25/10/2015', '09:00', 0, 0);

INSERT INTO Mora
VALUES (2, '555555555');
INSERT INTO Mora
VALUES (0, '444444444');
INSERT INTO Mora
VALUES (1, '666666666');

INSERT INTO EquipeAtleta
VALUES (0, '111111111');
INSERT INTO EquipeAtleta
VALUES (0, '222222222');
INSERT INTO EquipeAtleta
VALUES (1, '333333333');

INSERT INTO ModalidadeEvento
VALUES (0, 0);
INSERT INTO ModalidadeEvento
VALUES (0, 1);
INSERT INTO ModalidadeEvento
VALUES (0, 2);

INSERT INTO IngressoAtracao
VALUES (0, 0, 0, 'masculino');
INSERT INTO IngressoAtracao
VALUES (0, 0, 1, 'feminino');
INSERT INTO IngressoAtracao
VALUES (0, 0, 2, 'meia');

INSERT INTO Telefone
VALUES ('444444444', '(16)98882-1313');
INSERT INTO Telefone
VALUES ('444444444', '(16)91234-5555');
INSERT INTO Telefone
VALUES ('555555555', '(14)93232-2276');